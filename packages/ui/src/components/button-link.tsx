import { type Ref, forwardRef } from "react";
import { Link as AriaButton, type LinkProps } from "react-aria-components";
import { resolveClassName } from "../utilities/resolve-class-name.js";
import { type ButtonPreset, type ButtonShape, button } from "./button.js";

interface ButtonLinkProps extends LinkProps {
	preset?: ButtonPreset;
	thin?: boolean;
	shape?: ButtonShape;
}

export function _ButtonLink(
	{
		children,
		className,
		preset = "secondary",
		shape = "normal",
		thin = preset === "dimmed",
		...props
	}: ButtonLinkProps,
	ref: Ref<HTMLAnchorElement>,
) {
	return (
		<AriaButton
			ref={ref}
			{...props}
			className={(values) => {
				return button({
					className: resolveClassName(className, values),
					preset,
					shape,
					thin,
					...values,
				});
			}}
		>
			{children}
		</AriaButton>
	);
}

export const ButtonLink = forwardRef(_ButtonLink);
ButtonLink.displayName = "ButtonLink";
