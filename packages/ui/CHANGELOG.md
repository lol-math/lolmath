# @lolmath/ui

## 4.1.0

### Minor Changes

- 42c4e04: Export RouterProvider to allow for client-side routing customization. See also https://react-spectrum.adobe.com/react-aria/routing.html#app-router

### Patch Changes

- 66836c7: Use default cursor for disabled links
- a1e3c51: Fix select colors

## 4.0.0

### Major Changes

- 12aebbc: Use CSS modules instead of tailwind for internal component styles

## 3.2.5

### Patch Changes

- 67c5a5f: Tabs is a `use client` component.

## 3.2.4

### Patch Changes

- 9257833: Upgrade dependencies

## 3.2.3

### Patch Changes

- df0f027: switch to nodenext module resolution
- 3473fb2: Change file names of cjs exports

## 3.2.2

### Patch Changes

- b4c9c80: Use modern.js instead of tsup

## 3.2.1

### Patch Changes

- 4d8e119: Add extra prop forwarding for divider

## 3.2.0

### Minor Changes

- be713a2: Add Divider Component
- c4b3afe: Export gradients

## 3.1.1

### Patch Changes

- abe5adb: Change way border width is applied so that it works better with flex

## 3.1.0

### Minor Changes

- 41ba8a2: Add toggle button component

### Patch Changes

- cc316da: Make sure aspect ratio of square and round buttons is set properly for text based icons (icons with width smaller than height)

## 3.0.1

### Patch Changes

- 7b65269: remove background from accordion

## 3.0.0

### Major Changes

- 0b16e14: Adjust theme to be inline with https://brand.riotgames.com/en-us/league-of-legends/color/

### Minor Changes

- 0b16e14: Add typography components similar to branding guide

## 2.8.1

### Patch Changes

- e272d45: change number field, search input, slider, text area, text field so that they may receive children (labels)

## 2.8.0

### Minor Changes

- c54f762: Add squared, rounded button shapes
- c54f762: Add Card component
- c54f762: Add Tertiary button priority

## 2.7.0

### Minor Changes

- c9c5882: Initial version of Tooltip added

### Patch Changes

- 86914ce: slight change of colors of tooltips

## 2.6.0

### Minor Changes

- ec2a721: Add TextArea component

## 2.5.2

### Patch Changes

- 5c23331: forward classname of text field, search field, button

## 2.5.1

### Patch Changes

- 3cc0914: update package json and readme

## 2.5.0

### Minor Changes

- 1339caa: Add checkbox

### Patch Changes

- 6e90c02: Changed outline of tabs to not be visible by default, only when focus-visible.

## 2.4.1

### Patch Changes

- ba30c6f: forward class names

## 2.4.0

### Minor Changes

- 85d41cc: Add tabs

## 2.3.1

### Patch Changes

- 8d47fa4: re-add overflow to hide borders

## 2.3.0

### Minor Changes

- 963ce0f: Add view transitions to accordion component

## 2.2.0

### Minor Changes

- 43095eb: Add Modal

## 2.1.0

### Minor Changes

- 5b516bb: Add Spinner component

### Patch Changes

- e55dc68: Make label brighter

## 2.0.2

### Patch Changes

- b074d13: make sure groupProps is optional on number field

## 2.0.1

### Patch Changes

- f0550c0: infer type of slider (number or number array)

## 2.0.0

### Major Changes

- edff669: SliderLabel is now a separate component; removed from the main Slider component

### Minor Changes

- 5c864ab: Expose sliderTrackBackgroundClassName and sliderTrackForegroundClassName on Slider

## 1.3.0

### Minor Changes

- 6ca3bca: Add number field component

## 1.2.0

### Minor Changes

- 215447d: Add Radio Group
- 215447d: Add Label

### Patch Changes

- 215447d: Add utility to merge classes from userland

## 1.1.1

### Patch Changes

- 1e3e06f: make inputProps optional for both search field and text field

## 1.1.0

### Minor Changes

- 0e04991: add text field, change api of search field to match textfield (added inputProps prop)

## 1.0.3

### Patch Changes

- 1191873: add tailwindcss as peer dependency

## 1.0.2

### Patch Changes

- 8cf8309: Adjust package json publish config
