export type StatKey =
	| "baseAttackSpeed"
	| "attackSpeedRatio"
	| "movementSpeed"
	| "attackRange"
	| "hp"
	| "mana"
	| "armor"
	| "magicResist"
	| "hpRegen"
	| "manaRegen"
	| "critChance"
	| "attackDamage"
	| "bonusAttackSpeed";
