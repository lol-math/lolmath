export interface StatPreference {
	offense: number;
	defense: number;
	utility: number;
}
