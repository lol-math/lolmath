export type UpgradableAbilityKey = "Q" | "W" | "E" | "R";
export type AbilityKey = "P" | UpgradableAbilityKey;
