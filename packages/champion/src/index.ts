const championData = import.meta.glob("./champions/**/*.data.ts");
const championCalculation = import.meta.glob("./champions/**/*.calculation.ts");
