export enum AbilityDamage {
	physical = "physical",
	magical = "magical",
	true = "true",
	none = "none",
	shielding = "shielding",
	healing = "healing",
	mixed = "mixed",
}
