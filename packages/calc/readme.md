# Calc

A collection of functions for calculating League of Legends stats.

## Installation

```bash
npm install @lolmath/calc
```

## Links

- Docs: [https://docs.lolmath.net/calc](https://docs.lolmath.net/calc)
- Repository: [https://gitlab.com/lol-math/lolmath/-/tree/main/packages/calc](https://gitlab.com/lol-math/lolmath/-/tree/main/packages/calc)
- NPM: [https://www.npmjs.com/package/@lolmath/calc](https://www.npmjs.com/package/@lolmath/calc)