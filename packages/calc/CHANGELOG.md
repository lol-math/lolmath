# @lolmath/calc

## 0.0.7

### Patch Changes

- 9257833: Upgrade dependencies

## 0.0.6

### Patch Changes

- 8cf8309: Adjust package json publish config

## 0.0.5

### Patch Changes

- dd7c4c7: add types

## 0.0.4

### Patch Changes

- 584a5b4: Changed from vite to tsup. Only include esm.

## 0.0.3

### Patch Changes

- 09f8361: Add type declaration to build output

## 0.0.2

### Patch Changes

- 901bff0: Now includes build artifacts

## 0.0.1

### Patch Changes

- a08e213: Initial version of calc package
