/**
 * Computes the additive stacking of two values.
 *
 * @category Stacking
 *
 * @param initial The initial value.
 * @param value The value to add to the accumulator.
 * @returns The result of the addition.
 */
export function additive(initial: number, value: number) {
	return initial + value;
}
