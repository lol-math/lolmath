---
title: Projects
---

A list of public projects:

- **Item Optimizer**: The main website of lolmath.

  - [Repository](https://gitlab.com/lol-math/item-optimizer): Currently private,
    you can apply for access on discord (see link in footer).
  - [Website](https://lolmath.net)

- **@lolmath/calc**: A library for calculating the damage of League of Legends
  abilities.

  - [Repository](https://gitlab.com/lol-math/lolmath/-/tree/main/packages/calc)
  - [npm](https://www.npmjs.com/package/@lolmath/calc)

- **DDragon**: A wrapper for the Data Dragon, an API that serves League of
  Legends game data.

  - [Repository](https://gitlab.com/lol-math/ddragon)
  - [npm](https://www.npmjs.com/package/ddragon)

- **Ddragon WebP Images**: Just like Ddragon, but with WebP images. The images are
  hosted by us.

  - [Repository](https://gitlab.com/lol-math/ddragon-webp-images)
  - [npm](https://www.npmjs.com/package/ddragon-webp-images)

- **riot-proxy-cfw**: A Cloudflare Worker that proxies requests to the Riot API.

  - [Repository](https://gitlab.com/lol-math/riot-proxy-cfw)

- **bug-reports-cfw**: A Cloudflare Worker that proxies bug reports to GitLab.

  - [Repository](https://gitlab.com/lol-math/bug-reports-cfw)
