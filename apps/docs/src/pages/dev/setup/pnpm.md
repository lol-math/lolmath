We use [pnpm](https://pnpm.io/) as package manager.

It can be installed via `npm`:

```bash
npm install -g pnpm
```

Alternatively, you could also use the [installation instructions](https://pnpm.io/installation)
on their website.
