We use [Node.js](https://nodejs.org/en/) as runtime environment. In principle,
it should be possible to use any version of Node.js >= 16. However, we recommend
using the latest LTS version.

NodeJS can be installed via the [installation
instructions](https://nodejs.org/en/download/) on their website.

---

You can use [NVM](https://github.com/coreybutler/nvm-windows) to install and manage multiple versions of NodeJS.
This also makes it easier to upgrade with one command.
