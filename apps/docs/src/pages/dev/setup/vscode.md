---
title: VS Code
---

The recommended IDE for developing lolmath is [Visual Studio
Code](https://code.visualstudio.com/).

## Extensions

- Biome
- IntelliCode
- GitLens
- Tailwind CSS IntelliSense
